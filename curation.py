def curate(data):
    b = {}
    if not type(data) == dict:
        return data

    keys = data.keys()
    for a in keys:
        b[a.replace(".", "_")] = curate(data[a])
    return b
