from mongo import mongoConnection
from auth import oneDriveAuth
from curation import curate

client = oneDriveAuth.return_client()
db_collection = mongoConnection.get_client()

collection = client.item(drive='me', id='root').children.request(top=1000).get()

def collection_handler(collection):
    """
    Recusively download all the folders one by one
    """

    for x in collection:
        curated = curate(x.to_dict())
        curated["crawled"] = False
        db_collection.insert_one(curated)
        print u"saved {}, {}".format(curated["name"].encode('ascii', 'ignore').decode('ascii'), curated["id"])

        try:
            if curated["folder"]:
                print "going in {}".format(curated["name"])
                collection_handler(client.item(drive='me', id=curated["id"]).children.request(top=1000).get())
        except KeyError:
            pass

    try:
        collection = oneDriveAuth._get_another_collection(collection)
    except AttributeError, e:
        collection = False

    if collection:
        collection_handler(collection)

collection_handler(collection)
