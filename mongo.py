from pymongo import MongoClient


__all__ = [
    'mongoConnection'
]


class MongoConnection(object):

    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self._db()
        self._collection()

    def _db(self):
        self.db = self.client.onedrive

    def _collection(self):
        self.collection = self.db.files

    def get_client(self):
        return self.collection
mongoConnection = MongoConnection()
