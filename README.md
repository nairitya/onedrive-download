### Onedrive file downloader
1. Download all folders and files from onedrive. 
2. Download all folders at once. 
3. Resumable downloads.

### Why
1. Large zip file download usually fails in slow internet
2. Available clients are unreliable

### Working
1. Downloads the file details and save them into mongodb (by running ``fetcher.py``)
2. Start reading the file id from mongodb and downloads them from onedrive (by running ``downloader.py``

### Get in action
Create a Microsoft Developer App and update the App-id(client-id) and password(secret) to ``auth.py``

```
> $ pip install -r requirements.txt
> $ python fetcher.py
> $ python downloader.py
```
