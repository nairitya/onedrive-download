import onedrivesdk

api_base_url='https://api.onedrive.com/v1.0/'
scopes=['wl.signin', 'wl.offline_access', 'onedrive.readwrite']

__all__ = [
    'oneDriveAuth'
]


class OneDriveAuth(object):

    def __init__(self):
        self.client_id = '7d8f0fc2-e142-47dd-832e-c0ac2e8833ff'
        self.client_secret = 'QvoOV2bO0pMK5Uie8ykogJ6'
        self.redirect_uri = 'http://localhost:8080/'
        self.api_base_url = api_base_url
        self.scopes = scopes
        self._print_auth_url()
        self._get_code()

    def _print_auth_url(self):
        http_provider = onedrivesdk.HttpProvider()
        auth_provider = onedrivesdk.AuthProvider(
            http_provider=http_provider,
            client_id=self.client_id,
            scopes=self.scopes
        )

        self.client = onedrivesdk.OneDriveClient(self.api_base_url, auth_provider, http_provider)
        auth_url = self.client.auth_provider.get_auth_url(self.redirect_uri)
        print (auth_url)

    def _get_code(self):
        self.code = raw_input("Paste the code : ")
        # self.code = 'Mad332edd-6236-992b-4970-213222dde5cb'
        self.client.auth_provider.authenticate(self.code, self.redirect_uri, self.client_secret)

    def _get_another_collection(self, collection):
        return onedrivesdk.ChildrenCollectionRequest.get_next_page_request(collection, self.client).get()

    def return_client(self):
        return self.client

    def oned_sdk(self):
        return onedrivesdk
oneDriveAuth = OneDriveAuth()
