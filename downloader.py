import pymongo
from mongo import mongoConnection
from auth import oneDriveAuth
import onedrivesdk
import os

client = oneDriveAuth.return_client()
db_collection = mongoConnection.get_client()

all_data = db_collection.find({"crawled": False}, no_cursor_timeout=True).sort("name", pymongo.ASCENDING)

def handle_path(p):
    all_folders = p.split('/')
    if len(all_folders) <= 0:
        return

    current_path = "./data"
    for s_path in all_folders:
        try:
            current_path = current_path+s_path+"/"
            os.system(u"/bin/mkdir '{}'".format(current_path.replace("'", "_")))
        except:
            pass

for a in all_data:
    try:
        print "downloading {}".format(a["name"].encode('ascii', 'ignore').decode('ascii'))
        handle_path(a["parentReference"]["path"].split(":")[1])
        f_path = 'data'+a["parentReference"]["path"].split(":")[1]+"/"+a["name"].encode('ascii', 'ignore').decode('ascii')
        client.item(drive='me', id=a["id"]).download(f_path.replace("'", "_"))
        a["crawled"] = True
        db_collection.save(a)
        print "downloaded {}".format(a["name"].encode('ascii', 'ignore').decode('ascii'))
    except onedrivesdk.error.OneDriveError:
        a["crawled"] = True
        db_collection.save(a)
    except Exception, e:
        print "failed because of {}".format(str(e))
